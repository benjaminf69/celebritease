<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
class Category
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(targetEntity: Celebritease::class, mappedBy: 'category')]
    private Collection $celebriteases;

    public function __construct()
    {
        $this->celebriteases = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Celebritease>
     */
    public function getCelebriteases(): Collection
    {
        return $this->celebriteases;
    }

    public function addCelebritease(Celebritease $celebritease): static
    {
        if (!$this->celebriteases->contains($celebritease)) {
            $this->celebriteases->add($celebritease);
            $celebritease->setCategory($this);
        }

        return $this;
    }

    public function removeCelebritease(Celebritease $celebritease): static
    {
        if ($this->celebriteases->removeElement($celebritease)) {
            // set the owning side to null (unless already changed)
            if ($celebritease->getCategory() === $this) {
                $celebritease->setCategory(null);
            }
        }

        return $this;
    }
}
