<?php

namespace App\Controller;

use App\Entity\Celebritease;
use App\Entity\Celebrity; // Assurez-vous d'utiliser l'entité Celebrity correcte
use App\Form\CelebriteaseType;
use App\Form\CelebrityType; // Utilisez le nom correct de votre classe de formulaire
use App\Repository\CelebriteaseRepository;
use App\Repository\CelebrityRepository; // Utilisez le nom correct de votre repository
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class CelebriteaseController extends AbstractController // Assurez-vous que le nom de la classe correspond à votre utilisation
{
   public function __construct(
       private readonly CelebriteaseRepository $celebrityRepository,
       private readonly EntityManagerInterface $entityManager
   ){}

    #[Route('/app/celebritease', name: 'app_celebritease_index')]
    public function index(): Response
    {
        $celebriteases = $this->celebrityRepository->findAll();

        return $this->render('pages/celebritease/index.html.twig', [
            'celebriteases' => $celebriteases,
        ]);
    }

    /**
     * @throws \Exception
     */
    #[Route('/app/celebritease/edit/{id}', name: 'app_celebritease_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Celebritease $celebritease, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(CelebriteaseType::class, $celebritease);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $imageFile = $form->get('imageUrl')->getData();
            if ($imageFile) {
                $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$imageFile->guessExtension();

                try {
                    $imageFile->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    throw new \Exception('Error uploading image');

                }
                $celebritease->setImageUrl($newFilename);
            }

            $this->entityManager->persist($celebritease);
            $this->entityManager->flush();

            $this->addFlash('success', 'Celebrity updated successfully!');
            return $this->redirectToRoute('app_celebritease_index');
        }

        return $this->render('form/celebriteaseEditForm.html.twig', [
            'celebritease' => $celebritease,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/app/celebritease/new-form', name: 'app_celebritease_new', methods: ['GET', 'POST'])]
    public function newForm(Request $request, SluggerInterface $slugger): Response
    {
        $celebritease = new Celebritease();
        $form = $this->createForm(CelebriteaseType::class, $celebritease);
        $form->handleRequest($request);
        $user = $this->getUser();
        if ($form->isSubmitted() && $form->isValid()) {
            $imageFile = $form->get('imageUrl')->getData();
            if ($imageFile) {
                $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$imageFile->guessExtension();

                try {
                    $imageFile->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    throw new \Exception('Error uploading image');

                }
                $celebritease->setImageUrl($newFilename);
            }
            $celebritease->setOwner($user);
            $this->entityManager->persist($celebritease);
            $this->entityManager->flush();

            $this->addFlash('success', 'Celebrity created successfully!');
            return $this->redirectToRoute('app_celebritease_index');
        }

        return $this->render('form/celebriteaseNewForm.html.twig', [
            'celebritease' => $celebritease,
            'form' => $form->createView(),
        ]);
    }



    #[Route('/app/celebritease/delete/{id}', name: 'app_celebritease_delete', methods: ['POST', 'DELETE'])]
    public function delete(Request $request, Celebritease $celebritease): Response
    {
        if ($this->isCsrfTokenValid('delete'.$celebritease->getId(), $request->request->get('_token'))) {
            $this->entityManager->remove($celebritease);
            $this->entityManager->flush();
        }

        $this->addFlash('success', 'Celebrity deleted successfully!');
        return $this->redirectToRoute('app_celebritease_index');
    }

}
