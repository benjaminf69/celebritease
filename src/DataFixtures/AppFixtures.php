<?php

namespace App\DataFixtures;

use App\Entity\Celebritease;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $celebritease = new Celebritease();
        $celebritease->setName('Johnny Hallyday');
        $celebritease->setAlias('Johnny Vacances');
        $celebritease->setImageUrl("public/images/Celebritease/x1080.jpg");
        $manager->flush();
    }
}
