<?php
namespace App\EventSubscriber;

use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class UserVerifiedSubscriber implements EventSubscriberInterface
{
    private Security $security;
    private RouterInterface $router;
    private RequestStack $requestStack;

    public function __construct(Security $security, RouterInterface $router, RequestStack $requestStack)
    {
        $this->security = $security;
        $this->router = $router;
        $this->requestStack = $requestStack;
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $this->requestStack->getCurrentRequest();

        // Ignorer la racine ("/") et les sous-requêtes
        if (!$event->isMainRequest() || $request->getPathInfo() === '/') {
            return;
        }

        $user = $this->security->getUser();

        // Si l'utilisateur est connecté mais non vérifié
        if ($user instanceof User && !$user->getIsVerified()) {
            // Redirige uniquement les utilisateurs non vérifiés vers la page de connexion
            $response = new RedirectResponse($this->router->generate('app_login'));
            $event->setResponse($response);
        }

        // Pas besoin d'une autre action ici pour les utilisateurs vérifiés
        // La méthode s'arrête et la requête continue normalement
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
        ];
    }
}
